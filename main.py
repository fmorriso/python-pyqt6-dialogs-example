import decimal
import sys

from PyQt6.QtWidgets import *

from dummy_main_window import DummyMainWindow
from input_utilities import InputUtils


def get_python_version() -> str:
    return f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"


def verify_get_whole_number_with_dynamically_created_parent_window():
    title: str = 'Whole number test'
    msg: str = 'Enter a whole number'
    n: int = InputUtils.get_whole_number(title, msg)
    print(f'{n=}')


def verify_get_whole_number_with_user_created_parent_window():
    app = QApplication(sys.argv)
    w = DummyMainWindow()
    title: str = 'Whole number test'
    msg: str = 'Enter a whole number'
    n: int = InputUtils.get_whole_number(title, msg, app, w)
    print(f'{n=}')


def verify_get_decimal_number_with_dynamically_created_parent_window():
    title: str = 'Decimal number test'
    msg: str = 'Enter a decimal number'
    n: decimal = InputUtils.get_decimal_number(title, msg)
    decimal.getcontext().prec = 2
    print(f'{n=}')


if __name__ == '__main__':
    print(f'Python version: {get_python_version()}')
    # verify_dialog_with_dynamically_created_parent_window()
    verify_get_decimal_number_with_dynamically_created_parent_window()
    # FAILS: verify_get_whole_number_with_user_created_parent_window()
    verify_get_whole_number_with_dynamically_created_parent_window()

