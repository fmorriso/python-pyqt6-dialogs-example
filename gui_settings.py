from typing import ClassVar

import pyautogui
import pygame as pg
from pygame import Rect


class GuiSettings:
    ONE_TIME_COUNT: ClassVar[int] = 0  # for one-time print() calls

    __screen_pct: float = 0.0

    @property
    def screen_pct(self):
        return self.__screen_pct

    __device_width: int = 0

    @property
    def device_width(self) -> int:
        return self.__device_width

    __device_height: int = 0

    @property
    def device_height(self) -> int:
        return self.__device_height

    __scaled_width: int = 0

    @property
    def scaled_width(self) -> int:
        return self.__scaled_width

    __scaled_height: int = 0

    @property
    def scaled_height(self) -> int:
        return self.__scaled_height

    __screen_rectangle: Rect = None

    @property
    def screen_rectangle(self) -> Rect:
        return self.__screen_rectangle

    def __init__(self, pct: float, square: bool = False, multiple_of: int = 100):
        # calculate game size as a percentage of device screen size
        self.__device_width, self.__device_height = pyautogui.size()
        self.__screen_pct: float = float(pct)

        # round scaled width and height to multiple of 100 unless square needed in which case use height for both
        # since device height is usually the smaller of the two
        # TITLE_BAR_ESTIMATED_HEIGHT: int = 4
        self.__scaled_height: int = int(
            (self.device_height * self.__screen_pct // multiple_of) * multiple_of)
        self.__scaled_width: int = self.scaled_height if square else int(
            (self.device_width * self.__screen_pct // multiple_of) * multiple_of)

        if GuiSettings.ONE_TIME_COUNT == 0:
            print(f'device width: {self.device_width}, height: {self.device_height}')
            print(f'scaled width: {self.scaled_width}, height: {self.scaled_height}')

        self.__screen_rectangle: Rect = pg.Rect(0, 0, self.scaled_width, self.scaled_height)

        GuiSettings.ONE_TIME_COUNT += 1
